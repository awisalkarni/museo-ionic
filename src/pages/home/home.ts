import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';  
import { DetailPage } from '../detail/detail';
import { LoadingController } from 'ionic-angular'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {

  public playlist:any;
  

  constructor(public navCtrl: NavController, public httpClient: HttpClient, public loading: LoadingController) {
    this.loadData();
  }

  loadData(){
    let data:Observable<any>;
    let loader = this.loading.create({
      content: 'Getting latest entries...',
    });
    loader.present().then(() => {
      data = this.httpClient.get("http://museo.awislabs.com/api/latest");
      data.subscribe(result => {
        this.playlist = result;
      })
      loader.dismiss();
    });
  }

  ionViewLoaded() {
    
  }

  itemClicked(item){
    this.navCtrl.push(DetailPage, {
      item: item
    });
    
  }

}
