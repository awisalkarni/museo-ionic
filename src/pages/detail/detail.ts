import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})

export class DetailPage implements OnInit {



   public item:any;
   public youtubeURL:SafeResourceUrl;

  constructor(public navCtrl: NavController, private navParams: NavParams, private sanitizer: DomSanitizer) {
    
    this.item = this.navParams.data.item;
    console.log('item: '+ this.item)

  }

  ngOnInit(){
    this.youtubeURL = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/"+this.item.youtube_id);
  }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

}
